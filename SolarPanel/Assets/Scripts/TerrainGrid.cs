﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGrid : MonoBehaviour
{
    [SerializeField] private TerrainCollider terrainCollider; //can be deleted
    [SerializeField] private int hitLength = 200;

    private TerrainData terrainData;

    private void Start()
    {
        terrainData = terrainCollider.terrainData; //can be deleted

        float[,] heights = terrainData.GetHeights(0, 0, terrainData.heightmapWidth, terrainData.heightmapHeight);
        for (int i = 0; i < terrainData.heightmapWidth; i++)
        {
            heights[i, 0] = 10;
        }
        terrainData.SetHeights(0, 0, heights);
    }

    private void Update()
    {
        // Get touch position on terrain
        if (Input.GetButtonDown("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, hitLength, LayerMask.NameToLayer("Terrain")))
            {
                //Debug.Log(hit.point);
            }
        }

        
    }

    private void OnApplicationQuit()
    {
        float[,] heights = terrainData.GetHeights(0, 0, terrainData.heightmapWidth, terrainData.heightmapHeight);
        for (int i = 0; i < terrainData.heightmapWidth; i++)
        {
            for (int j = 0; j < terrainData.heightmapHeight; j++)
            {
                heights[i, j] = 0;
            }
        }
        terrainData.SetHeights(0, 0, heights);
    }

}
