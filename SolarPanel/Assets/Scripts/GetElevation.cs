﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class GetElevation : MonoBehaviour
{
    private string googleElevationApiKey;
    private string lat;
    private string lon;

    private List<double> dataElevation;

    private void Start()
    {
        InitializeData();
        GetDataFromGoogleElevationApi();

        //Debug.Log(dataElevation[0]);
    }

    private void InitializeData()
    {
        googleElevationApiKey = "AIzaSyAKWM8gsRBMjiDj6eMTjpcJqoFBpOum6NM";
        lat = "39.7391536";
        lon = "-104.9847034";

        dataElevation = new List<double>();
    }

    private void GetDataFromGoogleElevationApi()
    {
        ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

        using (WebClient wc = new WebClient())
        {
            string adress = "https://maps.googleapis.com/maps/api/elevation/json?locations=" + lat + "," + lon + "&key=" + googleElevationApiKey;
            var json = wc.DownloadString(adress);
            string[] splitData = json.Split(' ', ',');
            double dataToDouble = Convert.ToSingle(splitData[22]);
            double readyElevation = Math.Round(dataToDouble, 2);
            dataElevation.Add(readyElevation);
        }
    }

    private bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        bool isOk = true;
        // If there are errors in the certificate chain, look at each error to determine the cause.
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            for (int i = 0; i < chain.ChainStatus.Length; i++)
            {
                if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                    }
                }
            }
        }
        return isOk;
    }
}
