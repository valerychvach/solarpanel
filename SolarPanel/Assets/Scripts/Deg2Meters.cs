﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deg2Meters : MonoBehaviour {

    private void Start()
    {
        double lat1 = 49.785105 * Mathf.Deg2Rad;
        double lon1 = 23.999241 * Mathf.Deg2Rad;
        double lat2 = 49.785230 * Mathf.Deg2Rad;
        double lon2 = 24.003842 * Mathf.Deg2Rad;
        double R = 6371; // km

        double sLat1 = Math.Sin(lat1);
        double sLat2 = Math.Sin(lat2);
        double cLat1 = Math.Cos(lat1);
        double cLat2 = Math.Cos(lat2);
        double cLon = Math.Cos(lon1 - lon2);

        double cosD = sLat1 * sLat2 + cLat1 * cLat2 * cLon;

        double d = Math.Acos(cosD);

        double dist = R * d * 1000;

        Debug.Log(dist);

    }
}
